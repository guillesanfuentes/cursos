var brands = ['Toyota', 'Mazda', 'Renault']
var cars = []

function Car(brand, model, year) {
    this.brand = brand
    this.model = model
    this.year = year
}

for (var i = 1; i < 31; i++){
    cars.push(new Car(brands[Math.ceil((i/10)-1)], `Serie ${(i-1) % 10}`, 1999 + (i-1) % 10));
    console.log(i);
    console.log(cars[i-1]);
}
