var marca = ["Audi", "Subaru", "Lexus", "Porsche", "BMW", "Mazda", "Buick", "Toyota", "Kia", "Honda", "Hyundai", "Volvo", "Mini", "Mercedes-Benz", "Volkswagen", "Ford", "Lincoln", "Scion", "Acura", "Chevrolet", "Nissan", "Infiniti", "GMC", "Cadillac", "Dodge", "Land", "Rover", "Mitsubishi", "Jeep", "Fiat"];
var modelo = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30];
var annio = [2020,2019,2018,2020,2017,2020,2018,2018,2020,2020,2020,2018,2018,2020,2020,2019,2020,2020,2019,2019,2020,2020,2019,2019,2019,2020,2019,2019,2018,2020];
var listaAutos = [];

function autoN(marca, modelo, annio){
    this.marca = marca;
    this.modelo = modelo;
    this.annio = annio;
}

for (var i = 0; i<30; i++){
    var autoNuevo = new autoN(marca[i],modelo[i],annio[i]);
    listaAutos.push(autoNuevo);
}

console.log("-------------Filtrados-------------");

var autosFiltrados = listaAutos.filter(function(auto){
    return auto.annio === 2020;
});
console.log(autosFiltrados);

console.log("-------------Mapeado-------------");

var marcasRecientes = listaAutos.map(function(auto){
    return auto.marca;
});
console.log(marcasRecientes);

console.log("-----------ForEach Busqueda-----------");

var arrBusaqueda = [];
var encuentraArticulo = listaAutos.forEach(function(Auto){
  var x;
  if (Auto.annio === 2020){
    arrBusaqueda.push(Auto.marca)
  };
});
console.log(arrBusaqueda);

console.log("--------------ForEach 2.0--------------");

var encuentraArticulo = listaAutos.forEach(function(Auto){
  console.log(Auto.marca);
});

console.log("--------------Some--------------");

var boolEncontrar = listaAutos.some(function(Auto){
  return Auto.annio == 2017;
});
console.log(boolEncontrar);


console.log("----------------Listo-----------------");
